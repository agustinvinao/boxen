require boxen::environment
require homebrew
require gcc

Exec {
  group       => 'staff',
  logoutput   => on_failure,
  user        => $boxen_user,

  path => [
    "${boxen::config::home}/rbenv/shims",
    "${boxen::config::home}/rbenv/bin",
    "${boxen::config::home}/rbenv/plugins/ruby-build/bin",
    "${boxen::config::homebrewdir}/bin",
    '/usr/bin',
    '/bin',
    '/usr/sbin',
    '/sbin'
  ],

  environment => [
    "HOMEBREW_CACHE=${homebrew::config::cachedir}",
    "HOME=/Users/${::boxen_user}"
  ]
}

File {
  group => 'staff',
  owner => $boxen_user
}

Package {
  provider => homebrew,
  require  => Class['homebrew']
}

Repository {
  provider => git,
  extra    => [
    '--recurse-submodules'
  ],
  require  => File["${boxen::config::bindir}/boxen-git-credential"],
  config   => {
    'credential.helper' => "${boxen::config::bindir}/boxen-git-credential"
  }
}

Service {
  provider => ghlaunchd
}

class {
  'nodejs::global':
    version => 'v0.10'
}

Homebrew::Formula <| |> -> Package <| |>

node default {
  # core modules, needed for most things
  include dnsmasq
  include git
  include hub
  include nginx

  # fail if FDE is not enabled
  if $::root_encrypted == 'no' {
    fail('Please enable full disk encryption and try again')
  }

  # node versions
  # Set the global default node (auto-installs it if it can)
  class { 'nodejs:global':
    version => '0.12'
  }

  # ensure a npm module is installed for a certain node version
  # note, you can't have duplicate resource names so you have to name like so
  npm_module { "bower for ${version}":
    module       => 'bower',
    version      => '~> 1.4.1',
    node_version => '*',
  }
  
  # install a node version
  nodejs::version { '0.12.2': }

  # Set the global default ruby (auto-installs it if it can)
  class { 'ruby::global':
    version => '2.2.4'
  }

  # ensure a gem is installed for all ruby versions
  ruby_gem { 'bundler for all rubies':
    gem          => 'bundler',
    version      => '~> 1.0',
    ruby_version => '*',
  }

  # install a ruby version
  ruby::version { '2.2.4': }
  ruby::version { 'jruby-1.7.24': }

  # Installing rbenv plugin
  ruby::rbenv::plugin { 'rbenv-vars':
    ensure => 'v1.2.0',
    source  => 'sstephenson/rbenv-vars'
  }

  # # default ruby versions
  # ruby::version { '1.9.3': }
  # ruby::version { '2.0.0': }
  # ruby::version { '2.1.8': }
  # ruby::version { '2.2.4': }

  # common, useful packages
  package {
    [
      'ack',
      'findutils',
      'gnu-tar'
    ]:
  }

  file { "${boxen::config::srcdir}/our-boxen":
    ensure => link,
    target => $boxen::config::repodir
  }

  # include dropbox
  # include chrome
  # include handbrake
  # include postgresql
  # include sublime_text_2
  # include vlc
  # include wget
  # include ohmyzsh
  # include colloquy
  # include harvest
  # include googledrive
  # include nginx
  # include statsd
  # include ctags
  # include mongodb
  # include sequel_pro
  # include postgresapp
  # include skype
  # include redis
  # include mou
  # include mysql
  # include iterm2::stable
  # include the_silver_searcher
  # include reattachtousernamespace
  # include firefox
  # include colordiff
  # include libtool
  # include autoconf
  # include pkgconfig
  # include pcre
  # include elasticsearch
  # include java
  # include python
  # include archiver
  # include tmux
  # # include kafka
  # # include imagemagick

  repository {
    '/Users/agustinvinao/.dotfiles':
      source   => 'agustinvinao/dotfiles', #short hand for github repos
      provider => 'git';
  }

  # # Example of how you can manage your .vimrc
  # file { "/Users/${::boxen_user}/.vimrc":
  #   target  => "/Users/${::boxen_user}/.dotfiles/.vimrc",
  #   require => Repository["/Users/${::boxen_user}/.dotfiles"]
  # }


  # Example of how you can manage your .vimrc
  file { "/Users/${::boxen_user}/.tmux.conf":
    target  => "/Users/${::boxen_user}/.dotfiles/.tmux.conf",
    require => Repository["/Users/${::boxen_user}/.dotfiles"]
  }

  # Set the global version of Python
  class { 'python::global':
    version => '3.5.0'
  }

  python::version { '2.7.10': }
  python::version { '3.5.0': }
}
