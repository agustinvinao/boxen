[![Build Status](https://travis-ci.org/boxen/puppet-harvest.png?branch=master)](https://travis-ci.org/boxen/puppet-harvest)
# Harvest Puppet Module for Boxen

Installs the simple and quite useful (if you use Harvest) little Harvest time tracking client (http://www.getharvest.com/)

## Usage

```puppet
include harvest
```

## Required Puppet Modules

* `boxen`
