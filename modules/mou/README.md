# Mou Puppet Module for Boxen

Install [Mou](http://mouapp.com/), a lightweight Markdown client for Mac

## Usage

```puppet
include mou
```

## Required Puppet Modules

* `boxen`

## Development

Write code. Run `script/cibuild` to test it. Check the `script`
directory for other useful tools.